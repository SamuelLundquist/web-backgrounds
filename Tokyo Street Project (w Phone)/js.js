var clock_elem,date_elem,months = ["Jan","Feb","March","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
var backgroundList = ["night01.png","night01.png","night01.png","night01.png","night01.png","night01.png","sunset01.png","sunset01.png","sunset01.png","day01.png","day01.png","day01.png","day01.png","day01.png","day01.png","day01.png","day01.png","day01.png","day01.png","day01.png","sunset01.png","sunset01.png","night01.png","night01.png","night01.png"];
var use24HourClock = false;

function load(){
	clock_elem = document.getElementById("clock");
	date_elem = document.getElementById("date");
	dayCheck();

	setInterval(update,1000);//update every second
	setInterval(dayCheck, 300000/5);//update every minute
}

function update(){
	var d = new Date();
		
	clock_elem.innerHTML = ('0' + (use24HourClock ? d.getHours() : d.getHours() % 12 || 12)).slice(-2) + ":" + ('0' + d.getMinutes()).slice(-2);
		
	date_elem.innerHTML = months[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear();
}

function dayCheck(){
	var dayTime = new Date();
	
	var curHour = dayTime.getHours();
	
	var imageUrl = "url(./files/" + backgroundList[curHour] + ")";
	document.body.style.backgroundImage = imageUrl;
}