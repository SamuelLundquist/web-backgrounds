var backgroundList = ["night01.jpg","night01.jpg","night01.jpg","night01.jpg","night01.jpg","night01.jpg","sunset01.jpg","sunset01.jpg","day01.jpg","day01.jpg","day01.jpg","day01.jpg","day01.jpg","day01.jpg","day01.jpg","day01.jpg","day01.jpg","day01.jpg","day01.jpg","sunset01.jpg","sunset01.jpg","night01.jpg","night01.jpg","night01.jpg","night01.jpg"];


function load(){
	clock_elem = document.getElementById("clock");
	dayCheck();

	setInterval(update,1000);//update every second
	setInterval(dayCheck, 60000);//update every minute
}

function update()
{
	var d = new Date();
	clock_elem.innerHTML = ('' + d.getHours() % 12 || 12).slice(-2) + ":" + ('0' + d.getMinutes()).slice(-2);
}

function dayCheck()
{
	var dayTime = new Date();
	
	var curHour = dayTime.getHours();
	
	var imageUrl = "url(./files/" + backgroundList[curHour] + ")";
	document.body.style.backgroundImage = imageUrl;
}
