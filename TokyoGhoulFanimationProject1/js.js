var clock_elem,date_elem,months = ["Jan","Feb","March","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

function load(){
	clock_elem = document.getElementById("clock");
	date_elem = document.getElementById("date");

	setInterval(update,1000/30);
}

function update(){
	var curTime = new Date();
		
	clock_elem.innerHTML = ('0' + curTime.getHours()).slice(-2) + ":" + ('0' + curTime.getMinutes()).slice(-2) + ":" + ('0' + curTime.getSeconds()).slice(-2);
		
	date_elem.innerHTML = months[curTime.getMonth()] + " " + curTime.getDate() + ", " + curTime.getFullYear();
}