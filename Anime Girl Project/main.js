var clock_elem,date_elem,months = ["1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"];
var clock_elem,date_elem,dayofweek = ["(日)","(月)","(火)","(水)","(木)","(金)","(土)"];
var use24HourClock = true;

function load(){
	clock_elem = document.getElementById("clock");
	date_elem = document.getElementById("date");

	setInterval(update,1000);//updates every second
}

function update(){
	var d = new Date();
		
	clock_elem.innerHTML = ('0' + (use24HourClock ? d.getHours() : d.getHours() % 12 || 12)).slice(-2) + ":" + ('0' + d.getMinutes()).slice(-2);
		
	date_elem.innerHTML = d.getFullYear() + "年" + months[d.getMonth()] + d.getDate() + "日" + " " + dayofweek[d.getDay()];
}

$(document).mousemove(function(e){
				$('#wallpaper').css({left:-((e.pageX/2)/50)-(e.pageX*.03), top:(-(e.pageY/2)/50)-(e.pageY*.03)});
})
