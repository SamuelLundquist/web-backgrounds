var backgroundList = ["night01.jpg","night01.jpg","night01.jpg","night01.jpg","night01.jpg","night01.jpg","sunset01.jpg","sunset01.jpg","day01.jpg","day01.jpg","day01.jpg","day01.jpg","day01.jpg","day01.jpg","day01.jpg","day01.jpg","day01.jpg","day01.jpg","day01.jpg","sunset01.jpg","sunset01.jpg","night01.jpg","night01.jpg","night01.jpg","night01.jpg"];
var date_elem,dayofweek = ["日)","月)","火)","水)","木)","金)","土)"];
var use24HourClock = true;

function load(){
	clock_elem = document.getElementById("clock");
	date_elem = document.getElementById("date");
	dayCheck();

	setInterval(update,1000);//update every second
	setInterval(dayCheck, 60000);//update every minute
}

function update()
{
	var d = new Date();
	
	clock_elem.innerHTML = ('' + (use24HourClock ? '0' + d.getHours() : d.getHours() % 12 || 12)).slice(-2) + ":" + ('0' + d.getMinutes()).slice(-2);
	
	date_elem.innerHTML = d.getFullYear() + "年" + (d.getMonth()+1) + "月" + d.getDate() + "日 (" + dayofweek[d.getDay()];
}

function dayCheck()
{
	var dayTime = new Date();
	
	var curHour = dayTime.getHours();
	
	var imageUrl = "url(./files/" + backgroundList[curHour] + ")";
	document.body.style.backgroundImage = imageUrl;
}
